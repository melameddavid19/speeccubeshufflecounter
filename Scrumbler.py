#! usr/bin/python3.7
import random as rdm
from ScrambleDirectionComponent import ScrambleDirectionComponent


# es werden 25 Drehungen insgesammt benoetigt um den Cube vollstaendig zu mischen.
# verwende die random Funktion von MathPy

class Scrumbler():
	""" This is the docstring for this class. It is placed directly under the class """
	def __init__(self):
		self.directionComponents = {"L": "Left", "R": "Right", "U": "Up", "D": "Down", "F": "Front", "B": "Back"} # wird theoretisch nicht verwendet
		self.directionComponentList = ["L", "R", "U", "D", "F", "B"]
		self.directionModification = [" ", "'"]
		self.cycleCounter = "2"
		self.invert = False
		self.manipulationList = [] # Zwischenspeicher der fuer jeden Schritt verwendet wird

# Getter und Setter der Klasse
	def getDirectionComponents(self):
		""" This is the docstring for the class function. It is placed directly under the function-name """
		return self.directionComponents

	def setDirectionsList(self, newDirectionComponents):
		self.directionComponents = newDirectionComponents

	def getDirectionComponentList(self):
		return self.directionComponentList

	def setDirectionComponentList(self, newDirectionComponentList):
		self.directionComponentList = newDirectionComponentList

	def getDirectionModification(self):
		return self.directionModification

	def setDirectionModification(self, newDirectionMod):
		self.DirectionModification = newDirectionMod

	def getCycleCounter(self):
		return self.cycleCounter

	def setCycleCounter(self, newCycleCounter):
		self.cycleCounter = newCycleCounter

	def getInvert(self):
		return self.invert

	def setInvert(self, newInvert):
		self.invert = newInvert



	
	def randomModFaktor(self):
		""" Generate a random float value and returned this """
		modFaktor = rdm.random()
		return modFaktor


	def invertedModificationCalculator(self): 
		""" Generate modificator for directionComponents. Called randomModFaktor if retValue is under 0.4199 return False else return True. """
		if self.randomModFaktor() < 0.4199:
			return False
		else:
			return True


	def scrumbleDirections(self):
		""" Generate and return a list with 25 random directionComponents from directionComponentsList. """
		scrubledList = [] 

		for i in range(0, 25): # length of 25

			directionComponent = ScrambleDirectionComponent(self.getDirectionComponentList()[rdm.randint(0,5)], i)
			scrubledList.append(directionComponent)

# TODO: Entferne manipulationList als classMember und mache die wichtigen Listen jederzeit zugänglich
		# self.manipulationList = scrubledList
		return scrubledList


	def detectDoubleComponent(self, firstElem, secondElem):
		""" Compare two string of directionComponent to find doubles """
		firstElement = ScrambleDirectionComponent()
		firstElement.copyComponent(firstElem)
		secondElement = ScrambleDirectionComponent()
		secondElement.copyComponent(secondElem)

		if firstElement.getString() == secondElement.getString():
			return True
		else:
			return False 


	def reshuffleDoublesInComponents(self):
		""" Find all doubled values and replaced them with correkt value. """
		doubledComponent = "" # component that's doubled 
		replaceHolder = "" 

		# first-STEP: find the doubled values in self.manipulationList
		for i in range(0, 25): # if first arg is equal to the second in list
			
			if i < 24:
				firstArg = self.manipulationList[i]
				secondArg = self.manipulationList[i+1]
				
				if self.detectDoubleComponent(firstArg, secondArg):
					
					doubledComponent = firstArg 
					indexOfDoubled = doubledComponent.getNumber()

					# second-STEP: change doubled component
					randVal = rdm.random()
					copieDCL = ["L", "R", "U", "D", "F", "B"] # copieDCL = copie_Direction_Component_List
					newDCL = []

					# remove same component from DirectionComponentList
					print(doubledComponent)
					copieDCL.remove(doubledComponent.getString())
					newDCL = copieDCL

					# replace by random value
					if randVal < 0.2:
						replaceHolder = newDCL[0]
					elif randVal > 0.2 and randVal < 0.4:
						replaceHolder = newDCL[1]
					elif randVal > 0.4 and randVal < 0.6:
						replaceHolder = newDCL[2]
					elif randVal > 0.6 and randVal < 0.8:
						replaceHolder = newDCL[3]
					elif randVal > 0.8 and randVal < 0.999999999999999999:
						replaceHolder = newDCL[4]

					print(" The new one: " + replaceHolder)

					# replace replaceHolder with doubledElement in DirectionComponentList
					print()




			# third: check if replaced is not the same like before
				





""" 

	# Gebe den Richtungen eine Richtungsmodifikation
	def appendModification(self, directionsList):

		directionsWithMod = []
		randomListOfValues = rdm.sample(range(0, 25), 25)

		for listElement in randomListOfValues: # durchlauft 25 Mal

			invertedMod = self.modificationByFaktor()

			if invertedMod:
				directionsList[listElement] = str(directionsList[listElement]) + "'"
			
			directionsWithMod.append(directionsList[listElement])
		
		return directionsWithMod



	# Gebe den nicht-modifizierten Richtungen in der Liste einen zusaetzlichen Counter
	def appendCycleTimes(self, directionsWithMod):

		fianlList = []
		retVal = []
		notSignedElements = []

		#print(directionsWithMod)

		# Sammel alle Elemente ohne invertier-Zeichen
		for element in directionsWithMod:
			if "'" not in element:
				notSignedElements.append(element)

		# Iteriere durch diese um drehWert zu geben 
		for unSignedElem in range(0, len(notSignedElements)):
			combindElement = notSignedElements[unSignedElem] + str(self.cycleValueByFaktor())
			retVal.append(combindElement)
			
		# sammel alle Elemente in eine Liste
		for elem in directionsWithMod:
			retVal.append(elem)

		# packe alle Elemente in neuer reihenfolge in die finale-Liste
		newListShuffledOrder = rdm.sample(range(0, 25), 25)
		for finalElement in newListShuffledOrder:
			fianlList.append(retVal[finalElement])

		return fianlList


	# Funktion entfernt erstmal das doppelte Element, dann wird eine zufaellige Richtung zurueckgegeben.
	def changeElementByPossibility(self, elementToChange):
		# Um das selbe Element nicht nochmal setzen zu koennen, wird es aus der Liste entfernt
		listOfElements = self.getDirectionsList()
		# Zufaelliger Wert zwischen 1 und 1000 wird generiert
		possibility = rdm.randint(1, 1000) 
		# Element zum zurueckgeben
		returnElement = ""
		# ["L", "R", "U", "D", "F", "B"]

		# if-else Bedingung um neuen Wert zu erhalten
		if possibility <= 166:
			returnElement = listOfElements[0]
		elif possibility > 166 and possibility <= 332:
			returnElement = listOfElements[1]
		elif possibility > 332 and possibility <= 498:
			returnElement = listOfElements[2]
		elif possibility > 498 and possibility <= 664:
			returnElement = listOfElements[3]
		elif possibility > 664 and possibility <= 830:
			returnElement = listOfElements[4]
		else:
			returnElement = listOfElements[5]

		# Rekrusive pruefung ob eingangs Element gleich dem return Element
		if returnElement == elementToChange:
			#changeElementByPossibility(returnElement)
			pass
		else:
			return returnElement


			

	
	# entferne mehrere aufeinanderfolgende Elemente in einer Reihe
	def removeSameElements(self, finalList):

		# Wenn Element gleich dem naechsten Element ist, dann soll das Elementvertauscht werden.
		for element in finalList:
			#if element == finalList[element+1]:
			print(element)
		#		element = self.changeElementByPossibility(element)




	# finales mischen
	def shuffle(self):
		
		# holle alle noetigen Listen
		directionsList = self.shuffleDirections()
		modifDireList = self.appendModification(directionsList)
		modiCountyList = self.appendCycleTimes(modifDireList)
		finalOutput = self.removeSameElements(modiCountyList)
		
		# string als rueckgabe-Wert
		outPut = ""

		for element in finalOutput:
			outPut = outPut + element + " "

		return outPut """