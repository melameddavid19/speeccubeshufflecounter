class ScrambleDirectionComponent():
    """
    ScrambleDirectionComponent class to identify specific component in directionList.
    ScrambleDirectionComponent has two class members:
    1. the string with the letter for the direction
    2. the number with a number for the place identification
    """
    def __init__(self, astring="", anumber=0):
        self.string = astring
        self.number = anumber


    def getString(self):
        return self.string

    def setString(self, newString):
        self.string = newString

    def getNumber(self):
        return self.number

    def setNumber(self, newNumber):
        self.number = newNumber


    def copyComponent(self, component):
        "Method to copy specific ScrambleDirectionComponent-object"
        self.setString(component.getString())
        self.setNumber(component.getNumber())


    def __str__(self):
        return "self.string: " + str(self.getString()) + " | self.number: " + str(self.getNumber())

