#! usr/bin/python3.7
import random as rdm



# es werden 25 Drehungen insgesammt benoetigt um den Cube vollstaendig zu mischen.
# verwende die random Funktion von MathPy

class Scrumbler():
	""" This is the docstring for this class. It is placed directly under the class """
	def __init__(self):
		self.directionComponents = {"L": "Left", "R": "Right", "U": "Up", "D": "Down", "F": "Front", "B": "Back"}
		self.directionComponentList = ["L", "R", "U", "D", "F", "B"]
		self.directionModification = [" ", "'"]
		self.cycleCounter = "2"
		self.invert = False

# Getter und Setter der Klasse
	def getDirectionComponents(self):
		""" This is the docstring for the class function. It is placed directly under the function-name """
		return self.directionComponents

	def setDirectionsList(self, newDirectionComponents):
		self.directionComponents = newDirectionComponents

	def getDirectionComponentList(self):
		return self.directionComponentList

	def setDirectionComponentList(self, newDirectionComponentList):
		self.directionComponentList = newDirectionComponentList

	def getDirectionModification(self):
		return self.directionModification

	def setDirectionModification(self, newDirectionMod):
		self.DirectionModification = newDirectionMod

	def getCycleCounter(self):
		return self.cycleCounter

	def setCycleCounter(self, newCycleCounter):
		self.cycleCounter = newCycleCounter

	def getInvert(self):
		return self.invert

	def setInvert(self, newInvert):
		self.invert = newInvert


	
	def randomModFaktor(self):
		""" Generate a random float value and returned this """
		modFaktor = rdm.random()
		return modFaktor


	def invertedModificationCalculator(self): 
		""" Generate modificator for directionComponents. Called randomModFaktor if retValue is under 0.4199 return False else return True. """
		if self.randomModFaktor() < 0.4199:
			return False
		else:
			return True


	def scrumbleDirections(self):
		""" Generate and return a list with 25 random directionComponents from directionComponentsList. """
		scrubledList = [] 

		for i in range(0, 25): # length of 25

			randomMove = self.getDirectionComponentList()[rdm.randint(0,5)]
			scrubledList.append(randomMove)
		
		return scrubledList


	def detectDoubles(self, firstElement, secondElement):
		""" Detect double values """
		doubledValue = False

		if firstElement == secondElement:
			doubledValue = True
		else:
			doubledValue = True 


	def reshuffleDoublesInComponents(self, scrumbledList):
		""" Find all doubled values and replaced them with correkt value. """
		doubledComponent = "" # component that's doubled 
		replaceHolder = "" 
		# first: find the doubled values in scrubledList
		for i in range(0, 25): # if first arg is equal to the second in list
			firstArg = scrumbledList[i]
			secondArg = scrumbledList[i+1]
			if self.detectedDoubles(firstArg, secondArg):
				doubledComponent = firstArg 

			# second: replace component


			# third: check if replaced is not the same like before
				







	# Gebe den Richtungen eine Richtungsmodifikation
	def appendModification(self, directionsList):

		directionsWithMod = []
		randomListOfValues = rdm.sample(range(0, 25), 25)

		for listElement in randomListOfValues: # durchlauft 25 Mal

			invertedMod = self.modificationByFaktor()

			if invertedMod:
				directionsList[listElement] = str(directionsList[listElement]) + "'"
			
			directionsWithMod.append(directionsList[listElement])
		
		return directionsWithMod



	# Gebe den nicht-modifizierten Richtungen in der Liste einen zusaetzlichen Counter
	def appendCycleTimes(self, directionsWithMod):

		fianlList = []
		retVal = []
		notSignedElements = []

		#print(directionsWithMod)

		# Sammel alle Elemente ohne invertier-Zeichen
		for element in directionsWithMod:
			if "'" not in element:
				notSignedElements.append(element)

		# Iteriere durch diese um drehWert zu geben 
		for unSignedElem in range(0, len(notSignedElements)):
			combindElement = notSignedElements[unSignedElem] + str(self.cycleValueByFaktor())
			retVal.append(combindElement)
			
		# sammel alle Elemente in eine Liste
		for elem in directionsWithMod:
			retVal.append(elem)

		# packe alle Elemente in neuer reihenfolge in die finale-Liste
		newListShuffledOrder = rdm.sample(range(0, 25), 25)
		for finalElement in newListShuffledOrder:
			fianlList.append(retVal[finalElement])

		return fianlList


	# Funktion entfernt erstmal das doppelte Element, dann wird eine zufaellige Richtung zurueckgegeben.
	def changeElementByPossibility(self, elementToChange):
		# Um das selbe Element nicht nochmal setzen zu koennen, wird es aus der Liste entfernt
		listOfElements = self.getDirectionsList()
		# Zufaelliger Wert zwischen 1 und 1000 wird generiert
		possibility = rdm.randint(1, 1000) 
		# Element zum zurueckgeben
		returnElement = ""
		# ["L", "R", "U", "D", "F", "B"]

		# if-else Bedingung um neuen Wert zu erhalten
		if possibility <= 166:
			returnElement = listOfElements[0]
		elif possibility > 166 and possibility <= 332:
			returnElement = listOfElements[1]
		elif possibility > 332 and possibility <= 498:
			returnElement = listOfElements[2]
		elif possibility > 498 and possibility <= 664:
			returnElement = listOfElements[3]
		elif possibility > 664 and possibility <= 830:
			returnElement = listOfElements[4]
		else:
			returnElement = listOfElements[5]

		# Rekrusive pruefung ob eingangs Element gleich dem return Element
		if returnElement == elementToChange:
			#changeElementByPossibility(returnElement)
			pass
		else:
			return returnElement


			

	
	# entferne mehrere aufeinanderfolgende Elemente in einer Reihe
	def removeSameElements(self, finalList):

		# Wenn Element gleich dem naechsten Element ist, dann soll das Elementvertauscht werden.
		for element in finalList:
			#if element == finalList[element+1]:
			print(element)
		#		element = self.changeElementByPossibility(element)




	# finales mischen
	def shuffle(self):
		
		# holle alle noetigen Listen
		directionsList = self.shuffleDirections()
		modifDireList = self.appendModification(directionsList)
		modiCountyList = self.appendCycleTimes(modifDireList)
		finalOutput = self.removeSameElements(modiCountyList)
		
		# string als rueckgabe-Wert
		outPut = ""

		for element in finalOutput:
			outPut = outPut + element + " "

		return outPut